**On this page:**

[[_TOC_]]

# GitLab Architecture Experiment

## Context

This project is an experimentation to document the GitLab Architecture with code.  
It leverages the [Structurizr] tool and its [dsl].

[Structurizr]: https://structurizr.com/
[dsl]: https://github.com/structurizr/dsl/blob/master/docs/language-reference.md

This tool was chosen because it provides a clear separation between the structure (the "model") and
its representation (the "views").

See https://gitlab.com/gitlab-com/gl-security/security-architecture/general/-/issues/13 
and https://gitlab.com/gitlab-com/gl-security/security-architecture/general/-/issues/14 for more
details.

This POC was presented during the Security Show and Tell of May 25: 
https://docs.google.com/presentation/d/1iYwlQ72Hiv1y5IX6iW5f8gw_wcVSjdUb_LIDWcy6s_M/edit

## Layers

Defining the architecture of GitLab is a complex task, because GitLab has multiple faces. This POC
applies the concept defined in
https://gitlab.com/gitlab-com/gl-security/security-architecture/general/-/issues/14#note_1396624039:

```mermaid
graph BT
    Base[Base Architecture Interface]
    Archs[Architectures] -->|Implements| Base
    Deploys[Deployments] -->|Deploys| Archs
```

## Repository organization

### Base architecture

To reflect the layers defined above, the base architecture of GitLab is defined in the
[workspace.dsl](workspace.dsl) file. This file is representing the highest level of what Gitlab
is, meaning the Core of GitLab. All services are necessary in order to have GitLab up and running.
Everything else is considered an "extra feature".

The main architecture is composed of [Software
Systems](https://github.com/structurizr/dsl/blob/master/docs/language-reference.md#softwareSystem)
which are the highest level element of the C4 model. GitLab Core itself isn't modeled as a Software
System to have some space to expend containers with components (Reminder: the C4 model has only 4
levels).

This is a representation of this architecture:

![GitLab core architecture](images/GitLab-SystemLandscape.png)

## Architectures

The [structurizr/architectures](structurizr/architectures) folder holds all implementations, including Reference
Architectures, GitLab.com, Development (corresponding to
https://docs.gitlab.com/ee/development/architecture.html), ...

## Features

The [structurizr/features](structurizr/features/) folder holds all extra features (GitLab Runner,
Container Registry, GitLab Pages, ...). These features have been isolated on purpose: First of all,
they make the core architecture more complex, and they are not absolutely necessary to run GitLab.
Secondly, they are not covered by the reference architectures. Therefore, they become flexible
enough to have their own scaling architectures and documentation.

## Usage

Architectures are defined in `.dsl` files of this repository. These files are Structurizr
[workspaces](https://structurizr.com/help/workspaces). To render the main workspace, use the
Structurizr Docker image in the root folder of the repository:

```sh
$ alias structurizr='docker run -it --rm -p 8080:8080 -v $PWD:/usr/local/structurizr -e STRUCTURIZR_WORKSPACE_PATH structurizr/lite'
$ STRUCTURIZR_WORKSPACE_PATH=./structurizr/architectures/ra1k structurizr
$ open http://localhost:8080
```

Any modification to the `.dsl` files will generate new diagrams when the webpage is refreshed.

In the example above, the Reference Architecture 1k workspace will be used in Structurizr. To use
the base architecture, simply change the `STRUCTURIZR_WORKSPACE_PATH` variable when restarting the
service:

```sh
$ STRUCTURIZR_WORKSPACE_PATH=./structurizr/ structurizr
```

### Static website

While Structurizr Lite (Docker image above) is the best way to browse architectures, it can load
only one workspace at once. To present all the architectures of this project, a Hugo static website
is generated when commits are pushed to `main`:

[https://gitlab-com.gitlab.io/gl-security/security-architecture/gitlab-architecture/](https://gitlab-com.gitlab.io/gl-security/security-architecture/gitlab-architecture/)

To work on this website locally, generate the diagrams as png with the
`./scripts/export-workspaces.sh` script. This script will export all diagrams as `png` files and place
them in the `site/assets/diagrams` folder where they are required.

```sh
$ hugo server -s site
$ open http://localhost:1313
```

