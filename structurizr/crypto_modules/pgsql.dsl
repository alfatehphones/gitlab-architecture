component "pg_crypto" {
  technology "pg_crypto"
  description "https://github.com/postgres/postgres/tree/master/src/common"
  tags "crypto_module"
}
