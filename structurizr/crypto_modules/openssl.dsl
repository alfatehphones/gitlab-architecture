component "OpenSSL" {
  technology "OpenSSL"
  description "OpenSSL Crypto Module - https://www.openssl.org/"
  tags "crypto_module"
}
