workspace extends ../gitlab-core/workspace.dsl {

  !identifiers hierarchical

  model {

    docker_user = person "Docker user" {
        tags "new"
        description "Uses a Docker Daemon to pull and push images."
      }

    !ref services {
      container_registry = container "Container Registry" {
        technology "Go"
        description "Stateless, highly scalable server side application to store and distribute Docker images."
        !include ../../crypto_modules/go.dsl
        tags "new"
      }

      ingress.nginx -> container_registry "Forward requests" "https/5000" "tls"

      container_registry -> storage.object_storage "Upload/download blobs" "https/443" "tls"
      container_registry -> metadata_db.postgresql "Write/read metadata" "pgsql/5432" "tls"

      docker_user -> ingress.nginx "Pull/Push requests" "https/443" "tls"
    }
  }

  views {
    systemLandscape {
      include docker_user ingress services storage metadata_db
      autolayout lr
    }

    container services services_containers "Services containers" {
      include ->services.container_registry-> docker_user ingress.nginx
      autoLayout
    }

    container ingress ingress_containers "Ingress containers" {
      include docker_user ingress.nginx services.container_registry
      autoLayout
    }

    dynamic services {
      title "Token Authentication"
      # https://docs.gitlab.com/ee/administration/packages/container_registry.html#architecture-of-gitlab-container-registry
      # https://docs.docker.com/registry/spec/auth/token/
      docker_user -> ingress.nginx "Runs `docker login registry.gitlab.example`"
      ingress.nginx -> services.container_registry "Connects to the Registry backend pool"
      docker_user -> ingress.nginx "Connects to the GitLab API to obtains a token"
      ingress.nginx -> services.gitlab_workhorse
      services.gitlab_workhorse -> services.gitlab_rails "Signs the token with the registry key and hands it to the Docker client"
      docker_user -> ingress.nginx "Logs in again with the token received from the API"

      autoLayout lr
    }
  }
}
