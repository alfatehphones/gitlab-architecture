workspace extends ../../workspace.dsl

  model {
    !ref git_repos {
      !include ../../../../components/praefect.dsl
    }

    !ref storage {
      minIO = container "MinIO" {
         properties {
            project_URL "https://github.com/minio/minio/blob/master/README.md"
         }
      }

      # See https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/object_storage.metadata_db
      # and https://gitlab.com/gitlab-org/charts/gitlab/-/blob/master/doc/charts/gitlab/webservice/index.md#minio
      services.gitlab_workhorse -> minIO "Store and retrieve objects" "https/9000"
      services.gitlab_rails.puma -> minIO "Store and retrieve objects" "https/9000"
      services.sidekiq -> minIO "Store and retrieve objects" "https/443"
    }

    !ref state {
      # "The Redis setup in GDK is a standalone server."
      # https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/redis.md
    }

    development = deploymentEnvironment "GDK" {
      deploymentNode "Developer Laptop" {
        softwareSystemInstance git_repos
        softwareSystemInstance ingress
        softwareSystemInstance metadata_db
        softwareSystemInstance services
        softwareSystemInstance storage
        softwareSystemInstance state
      }
    }

    # TODO: Deployment with service discovery (Consul)
  }

  views {
    deployment * development "dev" {
      include *
      autolayout
    }

    deployment ingress development "ingress_dev" {
      include *
      autolayout
    }

    deployment git_repos development "git_repos_dev" {
      include *
      autolayout
    }

    container git_repos "Git_Containers" {
      include *
      autoLayout
    }

    container ingress "Ingress_Containers" {
      include *
      autoLayout
    }

    container services "Services_Containers" {
      include *
      autoLayout
    }

    container metadata_db "Metadata_Containers" {
      include *
      autoLayout
    }
  }
}
