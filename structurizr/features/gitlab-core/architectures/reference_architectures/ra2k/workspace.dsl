workspace extends ../../../workspace.dsl {

  model {
    # "RPC/8075" for non-tls
    services.gitlab_rails -> git_repos.gitaly "Git operations" "RPC/9999" "tls"
    services.gitlab_rails -> metadata_db.postgresql "GitLab metatada" "postgresql/5432" "tls"
    services.gitlab_rails -> state.redis "Cache and queue sidekiq jobs" "redis/6379" "tls"

    services.sidekiq -> metadata_db "GitLab metadata" "postgresql/5432" "tls"

    omnibus_deployment = deploymentEnvironment "Omnibus" {
      deploymentNode "loadbalancer node" {
        loadbalancer = infrastructureNode "Load Balancer"
      }

      postgresql = deploymentNode "PostgreSQL node" {
        softwareSystemInstance metadata_db
      }

      redis = deploymentNode "Redis node" {
        softwareSystemInstance state
      }

      gitaly = deploymentNode "Gitaly node" {
        softwareSystemInstance git_repos
      }

      gitlab_rails = deploymentNode "GitLab Rails node" {
        ingress_instance = softwareSystemInstance ingress
        softwareSystemInstance services
        instances 2
      }

      object_storage = deploymentNode "Object storage node" {
        softwareSystemInstance storage
      }

      loadbalancer -> ingress_instance "https/443" "tls"
    }

    # TODO: Cloud native environment with https://github.com/structurizr/dsl/blob/master/docs/language-reference.md#infrastructureNode
  }

  views {
    deployment * omnibus_deployment omnibus "Omnibus" {
      include *
      autolayout
    }

    container git_repos git_containers "Git containers" {
      include *
      autoLayout
    }

    container ingress ingress_containers "Ingress containers" {
      include *
      autoLayout
    }

    container metadata_db metadata_containers "Metadata containers" {
      include *
      autoLayout
    }
 
    container services services_containers "Services containers" {
      include *
      autoLayout
    }

    container storage storage_containers "Storage containers" {
      include *
      autoLayout
    }

    container state state_containers "State containers" {
      include *
      autoLayout
    }

  }
