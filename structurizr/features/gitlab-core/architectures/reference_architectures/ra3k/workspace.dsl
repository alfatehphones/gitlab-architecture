workspace extends ha_model.dsl {

  model {
    omnibus_deployment = deploymentEnvironment "Omnibus" {
      deploymentNode "External loadbalancer node" {
        external_loadbalancer = infrastructureNode "External Load Balancer"
      }

      deploymentNode "Internal loadbalancer node" {
        internal_loadbalancer = infrastructureNode "Internal Load Balancer" {
          perspectives {
            "Load Balancing" "The Internal Load Balancer is used to balance any internal connections the GitLab environment requires such as connections to PgBouncer and Praefect (Gitaly Cluster)."
          }
        }
      }

      postgresql_cluster = deploymentNode "PostgreSQL Cluster" {
        postgresql_node_primary = deploymentNode "PostgreSQL Primary" {
          containerInstance metadata_db.postgresql
        }
        postgresql_nodes_secondary = deploymentNode "PostgreSQL Secondary" {
          containerInstance postgresql_secondary
          instances 2
        }

        pgbouncer_nodes = deploymentNode "PgBouncer nodes" {
          containerInstance pgbouncer
          instances 3
        }
      }

      redis = deploymentNode "Redis nodes" {
        containerInstance state.redis
        instances 3
      }

      consul_sentinel = deploymentNode "Consul nodes" {
        containerInstance sentinel
        containerInstance consul
        instances 3
      }

      gitaly_cluster = deploymentNode "Gitaly Cluster" {
        gitaly_nodes = deploymentNode "Gitaly nodes" {
          containerInstance git_repos.gitaly
          instances 3
        }

        praefect_nodes = deploymentNode "Praefect nodes" {
          containerInstance praefect {
            perspectives {
              "Load Balancing" "Must be behind a Load Balancer"
            }
          }
          instances 3
        }

        praefect_postgresql_node = deploymentNode "Praefect PostgreSQL" {
          containerInstance praefect_postgresql
        }
      }

      services_cluster = deploymentNode "Services Cluster" {
        gitlab_rails_nodes = deploymentNode "GitLab Rails nodes" {
          ingress_instance = softwareSystemInstance ingress
          containerInstance services.gitlab_workhorse
          gitlab_rails_cont_inst = containerInstance services.gitlab_rails
          instances 3
        }

        gitlab_sidekiq_nodes = deploymentNode "GitLab Sidekiq nodes" {
          sidekiq_cont_inst = containerInstance services.sidekiq
          instances 2
        }
      }

      object_storage = deploymentNode "Object storage node" {
        softwareSystemInstance storage
      }

      external_loadbalancer -> ingress_instance "https/443" "tls"
      internal_loadbalancer -> praefect_nodes "RPC/3305" "tls" {
          perspectives {
            "Load Balancing" "Load Balanced connection"
          }
      }
      internal_loadbalancer -> pgbouncer_nodes "postgresql/6432" "tls" {
          perspectives {
            "Load Balancing" "Load Balanced connection"
          }
        }
      gitlab_rails_cont_inst -> internal_loadbalancer "GitLab Metadata" "postgresql/6432" "tls" {
        perspectives {
          "Load Balancing" "Load Balanced connection"
        }
      }
      gitlab_rails_cont_inst -> internal_loadbalancer "Git Operations" "3305" "tls" {
        perspectives {
          "Load Balancing" "Load Balanced connection"
        }
      }
      sidekiq_cont_inst -> internal_loadbalancer "GitLab Metadata" "postgresql/6432" "tls" {
        perspectives {
          "Load Balancing" "Load Balanced connection"
        }
      }
      sidekiq_cont_inst -> internal_loadbalancer "Git operations" "postgresql/6432" "tls" {
        perspectives {
          "Load Balancing" "Load Balanced connection"
        }
      }
    }

    # TODO: Cloud native environment with https://github.com/structurizr/dsl/blob/master/docs/language-reference.md#infrastructureNode
  }

  views {
    deployment * omnibus_deployment omnibus "Omnibus (without Object Storage)" {
      include *
      exclude object_storage
      # We have introduced an internal loadbalancer in this deployment, so we need to hide loadbalanced connections
      exclude internal_loadbalancer
      autolayout
    }

    deployment * omnibus_deployment omnibus_with_os "Omnibus (with Object Storage)" {
      include *
      exclude relationship.tag==loadbalanced
      autolayout
    }
 
    deployment * omnibus_deployment omnibus_services "Omnibus Services Cluster" {
      include services_cluster
      autolayout
    }

    deployment * omnibus_deployment omnibus_postgresql "Omnibus PostgreSQL Cluster" {
      include postgresql_cluster
      autolayout
    }

    deployment * omnibus_deployment omnibus_gitaly "Omnibus Gitaly Cluster" {
      include gitaly_cluster
      autolayout
    }

    container git_repos git_containers "Git containers" {
      include *
      autoLayout
    }

    container ingress ingress_containers "Ingress containers" {
      include *
      autoLayout
    }

    container metadata_db metadata_containers "Metadata containers" {
      include *
      autoLayout
    }
 
    container services services_containers "Services containers" {
      include *
      autoLayout
    }

    container storage storage_containers "Storage containers" {
      include *
      autoLayout
    }

    container state state_containers "State containers" {
      include *
      autoLayout
    }
  }
}
