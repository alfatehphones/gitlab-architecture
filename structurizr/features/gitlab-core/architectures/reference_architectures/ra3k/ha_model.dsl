workspace extends ../../../workspace.dsl {

model {
    !ref metadata_db {
      !include ../../../../../components/consul.dsl
    }

    !ref state {
      !include ../../../../../components/sentinel.dsl
    }

    !ref metadata_db {
      pgbouncer = container "PgBouncer"

      services.gitlab_rails -> pgbouncer "GitLab Metadata (LoadBalanced)" "postgresql/6432" {
        tags "tls" "loadbalanced"
        properties {
          protocol "postgresql"
        }
      }

      services.sidekiq -> pgbouncer "GitLab Metadata (LoadBalanced)" "postgresql/6432" {
        tags "tls" "loadbalanced"
        properties {
          protocol "postgresql"
        }
      }

      pgbouncer -> metadata_db.postgresql "GitLab Metadata" "5432" "tls" {
        properties {
          protocol "postgresql"
        }
      }

      postgresql_secondary = container "PostgreSQL secondary"

      metadata_db.postgresql -> postgresql_secondary "Replication" "5432" {
        properties {
          protocol "postgresql"
        }
      }
    }

    !ref git_repos {
      !include ../../../../../components/praefect.dsl
    }

    # https://docs.gitlab.com/ee/administration/gitaly/praefect.html#network-latency-and-connectivity
    services.gitlab_rails -> praefect "Git operations (LoadBalanced)" "RPC/3305" {
      tags "tls" "loadbalanced"
    }

    # https://docs.gitlab.com/ee/administration/gitaly/praefect.html#network-latency-and-connectivity
    services.sidekiq -> praefect "Git operations (LoadBalanced)" "RPC/3305" {
      tags "tls" "loadbalanced"
    }
  }
}
