# 2. Group containers in Software Systems

Date: 2023-06-07

## Status

Accepted

## Context

GitLab Core is composed of different services. These services can take different forms depending on
how and where GitLab is deployed. For example, a PostgreSQL database is required to run GitLab and
it can be vary from a single server to a cluster with PgBouncer load balancing requests.

We need a way to hide this complexity while leaving the model flexible enough to cover all these
variations.

## Decision

It was decided to use [Software Systems](https://github.com/structurizr/dsl/blob/master/docs/language-reference.md#softwareSystem) to represent these services.
GitLab is composed of these Software Systems:

1. Ingress: Should be the only ingress to GitLab Core.
1. Services: Back end services like the Ruby on Rails monolith, Sidekiq, Workhorse, etc.
1. Git Repos: Storage of git data.
1. State: Cache, queues, and discoverability (for HA architectures).
1. Metadata DB: PostgreSQL database
1. Storage: Object Storage

[Groups](https://github.com/structurizr/dsl/blob/master/docs/language-reference.md#group) were
considered as well but not retained because they cannot be extended with the
[`!ref`](https://github.com/structurizr/dsl/blob/master/docs/language-reference.md#ref) keyword,
while Software Systems can be extended like this:


```
workspace extends path_to_the_core_architecure/workspace.dsl
model {
  !ref git_repos {
    praefect = container "Praefect" {
      properties {
        project_URL "https://gitlab.com/gitlab-org/gitaly/blob/master/README.md"
      }
    }
    postgresql = container "PostgreSQL"

    praefect -> postgresql "Store and retrieve git metatdata" "pgsql/5432"
    praefect -> git_repos.gitaly "Proxy and coordonate replication" "RPC/9999" "tls"
  }
}
```

This means we can define a high level model of the GitLab Core architecture while leaving the
details to the implementations (example: Reference Architecture 1K).

## Consequences

GitLab cannot be represented as a single Software System anymore. It makes it impossible to extend
the Core workspace and represent GitLab as a whole. This will make some diagrams a bit more complex
than necessary, but at the same time it will force us to define precisely how other services are
interacting with it. Likely, they will only connect to the Ingress Software System explicitly.
