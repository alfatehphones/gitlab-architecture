workspace {

  # avoid collision between element identifiers
  # https://github.com/structurizr/dsl/blob/master/docs/language-reference.md#identifier-scope
 !identifiers hierarchical

 !adrs ./docs/decisions/

  model {
    user = person "User"

    group "GitLab" {
      ingress = softwareSystem "Ingress" {
        nginx = container "Nginx" {
          !include ../../crypto_modules/openssl.dsl
        }
        gitlab_shell = container "Gitlab Shell" {
          technology "Go"
          !include ../../crypto_modules/go.dsl
        }
      }

      services = softwareSystem "Services" {
        gitlab_workhorse = container "GitLab Workhorse" {
          technology "Go"
          !include ../../crypto_modules/go.dsl
        }
        sidekiq = container "Sidekiq" {
          technology "Ruby on Rails"
        }
        gitlab_rails = container "Gitlab Rails" {
          technology "Ruby on Rails"
          !include ../../crypto_modules/openssl.dsl

          puma = component "Puma"
        }

        gitlab_workhorse -> gitlab_rails "Forward requests" "https/443"
      }

      storage = softwareSystem "Storage" {
        tags "storage"
        # One of https://docs.gitlab.com/ee/administration/object_storage.html#supported-object-storage-providers
        object_storage = container "Object Storage" {
          tags "storage"
        }

        services.gitlab_workhorse -> storage.object_storage "Uses" "https/443" "tls" {
          properties {
            "port" 443
            "protocol" "https"
            "transport" "TCP"
            "encryption" "tls"
          }
        }
        services.gitlab_rails -> object_storage "GitLab metadata" "https/443" "tls" {
          properties {
            "port" 443
            "protocol" "https"
            "transport" "TCP"
            "encryption" "tls"
          }
        }
        services.sidekiq -> object_storage "GitLab metadata" "https/443" "tls" {
          properties {
            "port" 443
            "protocol" "https"
            "transport" "TCP"
            "encryption" "tls"
          }
        }
      }

      state = softwareSystem "State" {
        tags "queue"
        redis = container "Redis" {
          tags "queue"
          technology "redis"
          !include ../../crypto_modules/openssl.dsl
        }
      }

      metadata_db = softwareSystem "Metadata DB" {
        tags "database"
        postgresql = container "PostgreSQL" {
          tags "database"
          technology "PostgreSQL"
          !include ../../crypto_modules/pgsql.dsl
        }
      }

      git_repos = softwareSystem "Git Repos" {
        tags "git"
        gitaly = container "Gitaly"
        gitaly -> storage.object_storage "Git data" "https/443" "tls" {
          properties {
            "port" 443
            "protocol" "https"
            "transport" "TCP"
            "encryption" "tls"
          }
        }
      }
    }

    user -> ingress "Uses UI" "https/443" "tls" {
      properties {
        "port" 443
          "protocol" "https"
          "transport" "TCP"
          "encryption" "tls"
      }
    }
    user -> ingress "GIT over SSH" "ssh/22" "tls"
    user -> ingress "GIT over HTTPS" "https/443" "tls"

    ingress -> services "Forward requests" "HTTPS"
    ingress -> git_repos "Handles Git SSH sessions" "ssh"
    services -> metadata_db "Store and retrieve metadata" "Postgresql"
    services -> git_repos "Store and retrieve git repos" "git"
    services -> state "Cache + Sidekiq jobs" "git"
    services.sidekiq -> state "Get jobs"

    ingress.gitlab_shell -> services.gitlab_workhorse "Uses" "https/443" "tls"

    services.gitlab_rails -> git_repos

    ingress.nginx -> services.gitlab_workhorse "Forward requests" "https/443" "tls"

    services.gitlab_workhorse -> services.gitlab_rails.puma "Uses" "https/443" "tls"

  }

  views {
    # Define views in implementated architectures
    # Not defining views here will automatically generate:
    # 1 x System Landscape, 1 x System Context, 1 x Container - all with auto-layout enabled
    # https://github.com/structurizr/dsl/blob/master/docs/language-reference.md#convention-over-configuration-useful-defaults

    theme default

    styles {
      element "database" {
        shape Cylinder
        icon "../../icons/postgresql.png"
      }

      element "queue" {
        shape Pipe
        icon "../../icons/redis.png"
      }

      element "new" {
        background red
      }

      element "storage" {
        shape Folder
      }

      element "git" {
        icon "../../icons/git.png"
      }

      element "Group:GitLab" {
        icon "../../icons/gitlab.png"
      }

      relationship "loadbalanced" {
        thickness 2
      }

    }
  }
}
