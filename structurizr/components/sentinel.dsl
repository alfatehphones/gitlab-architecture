sentinel = container "Sentinel"

sentinel -> state.redis "Redis protocol" "6379" "tls"
services.gitlab_rails.puma -> sentinel "Cache and queue sidekiq jobs" "26379" "tls"
services.sidekiq -> sentinel "Fetch queued jobs" "26379" "tls"
