consul = container "Consul"

# See https://gitlab.com/gitlab-org/quality/reference-architectures/-/issues/116/#note_851858727
consul -> consul "SERF health check (UDP)" "8301"
# 8500 for plain http
services.gitlab_rails.puma -> consul "HTTP traffic (TCP)" "https/8501" "tls"
services.gitlab_rails.puma -> consul "DNS (UDP/TCP)" "8600"
