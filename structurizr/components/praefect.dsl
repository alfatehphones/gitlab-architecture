praefect = container "Praefect" {
  properties {
    project_URL "https://gitlab.com/gitlab-org/gitaly/blob/master/README.md"
  }
}
postgresql = container "PostgreSQL"

praefect -> postgresql "Store and retrieve git metatdata" "pgsql/5432"
# https://docs.gitlab.com/ee/administration/gitaly/praefect.html#network-latency-and-connectivity
praefect -> git_repos.gitaly "Proxy and coordonate replication" "RPC/9999" "tls"

praefect_postgresql = container "Praefect PostgreSQL" {
  properties {
    fault-tolerant false
  }
}

praefect -> praefect_postgresql "Git Metadata" "5432" "tls"
