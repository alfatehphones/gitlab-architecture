#!/bin/bash

# strict mode - http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail

SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

function export_workspace() {
  trap "cd - 1>/dev/null ; docker rm -f structurizr_export 1>/dev/null" RETURN EXIT
  local workspace_path=$(dirname $1)
  docker run -it --rm -p 8080 -v $PWD:/usr/local/structurizr --name structurizr_export -e STRUCTURIZR_WORKSPACE_PATH=$workspace_path -d structurizr/lite 1>/dev/null
  # get container port
  port=$(docker port structurizr_export 8080 | awk -F':' 'NR==1{print $2}')
  echo -n "  Waiting for structurizr to be up and running"
  until curl -f -s -o /dev/null http://localhost:${port}; do
    echo -n "."
    sleep 1
  done
  echo "up"
  # Create export folder while stripping the structurizr/ prefix of the path
  mkdir -p export/${workspace_path#./structurizr}
  cd export/${workspace_path#./structurizr}
  node $SCRIPTPATH/export-diagrams.js http://localhost:${port}/workspace/diagrams
}

function install_puppeteer() {
  if ! npm list | grep puppeteer -q; then
    npm install puppeteer
  fi
}

function clean_up() {
  rm -rf export/
  mkdir export
}

function export_all() {
  find ./structurizr -name 'workspace.dsl' -print0 |
    while IFS= read -r -d '' workspace; do
      echo "Exporting $workspace"
      export_workspace $workspace
    done
}

function copy_diagrams_to_site() {
  rm -rf site/assets/diagrams
  cp -r export site/assets/diagrams
}

function copy_workspaces_json_to_site() {
  rm -rf site/assets/workspaces
  mkdir -p site/assets/workspaces/
  find ./structurizr -name 'workspace.json' -print0 |
    while IFS= read -r -d '' workspace; do
      local workspace_path=$(dirname $workspace)
      mkdir -p site/assets/workspaces/${workspace_path#./structurizr}
      cp $workspace site/assets/workspaces/${workspace_path#./structurizr}
    done
}

# TODO: replace this with a container from ghcr.io/puppeteer/puppeteer
install_puppeteer
if [[ -z "${1-}" ]]; then
  clean_up
  export_all
else
  export_workspace "$1"
fi
copy_diagrams_to_site
copy_workspaces_json_to_site
