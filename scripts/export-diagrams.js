const puppeteer = require('puppeteer');
const fs = require('fs');

const IGNORE_HTTPS_ERRORS = true;
const HEADLESS = "new";

if (process.argv.length < 3) {
  console.log("Usage: <structurizrUrl>")
  process.exit(1);
}

const url = process.argv[2];

var expectedNumberOfExports = 0;
var actualNumberOfExports = 0;

(async () => {
  const browser = await puppeteer.launch({
    ignoreHTTPSErrors: IGNORE_HTTPS_ERRORS,
    headless: HEADLESS
  });
  const page = await browser.newPage();

  // visit the diagrams page
  console.log("  Opening " + url);
  await page.goto(url, {
    waitUntil: 'domcontentloaded'
  });
  await page.waitForFunction('structurizr.scripting && structurizr.scripting.isDiagramRendered() === true');

  // add a function to the page to save the generated PNG images
  await page.exposeFunction('savePNG', (content, filename) => {
    console.log("   - " + filename);
    content = content.replace(/^data:image\/png;base64,/, "");
    fs.writeFileSync(filename, content, 'base64', function(err) {
      if (err) throw err;
    });

    actualNumberOfExports++;

    if (actualNumberOfExports === expectedNumberOfExports) {
      console.log(" - Finished");
      browser.close();
    }
  });

  // get the array of views
  const views = await page.evaluate(() => {
    return structurizr.scripting.getViews();
  });

  expectedNumberOfExports =  views.length

  console.log("  Starting export");
  for (var i = 0; i < views.length; i++) {
    const view = views[i];

    await page.evaluate((view) => {
      structurizr.scripting.changeView(view.key);
    }, view);

    await page.waitForFunction('structurizr.scripting.isDiagramRendered() === true');

    const diagramFilename = view.key + '.png';

    await page.evaluate((diagramFilename) => {
      structurizr.scripting.exportCurrentDiagramToPNG({
        includeMetadata: true,
        crop: false
      }, function(png) {
          window.savePNG(png, diagramFilename);
        })
    }, diagramFilename);
  }
})();
