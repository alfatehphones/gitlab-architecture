---
title: "GitLab Architecture Experiment"
draft: false
---

## Context

This project is an experimentation to document the GitLab Architecture with code.  
It leverages the [Structurizr] tool and its [dsl].

[Structurizr]: https://structurizr.com/
[dsl]: https://github.com/structurizr/dsl/blob/master/docs/language-reference.md

This tool was chosen because it provides a clear separation between the structure (the "model") and
its representation (the "views").

See https://gitlab.com/gitlab-com/gl-security/security-architecture/general/-/issues/13 
and https://gitlab.com/gitlab-com/gl-security/security-architecture/general/-/issues/14 for more
details.

This POC was presented during the Security Show and Tell of May 25: 
https://docs.google.com/presentation/d/1iYwlQ72Hiv1y5IX6iW5f8gw_wcVSjdUb_LIDWcy6s_M/edit

## Layers

Defining the architecture of GitLab is a complex task, because GitLab has multiple faces. This POC
extends the concept defined in
https://gitlab.com/gitlab-com/gl-security/security-architecture/general/-/issues/14#note_1396624039
to:

```mermaid
graph TB
    GitLab -->|Has many| Features
    Features -->|Has one| Base[Base Architecture Interface] 
    Base -->|Has many| Archs[Architectures]
    Archs -->|Has many| Deploys[Deployments]
```

GitLab is a single product composed of many different features organized around what we call here
"GitLab-Core". GitLab-Core is the minimum set of components and services needed to run an instance.
