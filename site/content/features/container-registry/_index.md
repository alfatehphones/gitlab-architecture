---
title: "Container Registry"
refs: "https://docs.gitlab.com/ee/user/packages/container_registry/"
sbom: "https://gitlab.com/gitlab-org/sbom/reports/-/tree/main/sbom/container-registry/"
layout: "architecture"
---

### System landscape

{{< diagram name="container-registry/SystemLandscape-001.png" >}}

### Token authentication

{{< diagram name="container-registry/Dynamic-001.png" >}}

### Ingress

{{< diagram name="container-registry/ingress_containers.png" >}}

### Services

{{< diagram name="container-registry/services_containers.png" >}}
