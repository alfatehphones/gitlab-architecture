---
title: "GitLab Core"
sbom: "https://gitlab.com/gitlab-org/sbom/reports/-/tree/main/sbom/gitlab-core/"
weight: 1
layout: "architecture"
---

## System landscape

GitLab base architecture is an abstract architecture with only high level components (Software
Systems in Structurizr):

{{< diagram name="gitlab-core/SystemLandscape-001.png" >}}

Because it's an abstract architecture, this page doesn't describe any particular implementation.

## GitLab Core

Components of this base architecture and its implementations are considered part of "GitLab Core".
GitLab Core is the minimum set of components and services needed to start a GitLab instance.

Some key projects and components like gitlab-runner are not part of Gitlab Core because they're not strictly
necessary to boot up GitLab. These components are described in the [features]({{< ref "features">}}) section.

## Ingress

{{< diagram name="gitlab-core/SystemContext-002.png" >}}

### Ingress Container

{{< diagram name="gitlab-core/Container-002.png" >}}

## Services

{{< diagram name="gitlab-core/SystemContext-004.png" >}}

### Services-Container

{{< diagram name="gitlab-core/Container-004.png" >}}

### Services Gitlab Rails Component

{{< diagram name="gitlab-core/Component-005.png" >}}

## Git Repos

{{< diagram name="gitlab-core/SystemContext-001.png" >}}

### GitRepos Container

{{< diagram name="gitlab-core/Container-001.png" >}}

## State

{{< diagram name="gitlab-core/SystemContext-005.png" >}}

## Metadata DB

{{< diagram name="gitlab-core/SystemContext-005.png" >}}

## Storage

{{< diagram name="gitlab-core/SystemContext-006.png" >}}
