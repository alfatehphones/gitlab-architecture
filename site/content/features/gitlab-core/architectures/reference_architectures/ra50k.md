---
title: "RA50K"
weight: 50
refs: "https://docs.gitlab.com/ee/administration/reference_architectures/25k_users.html"
layout: "architecture"
---

{{< alert style="info" >}}The RA50K is the same as the [RA25K](../ra25k) just with larger VMs and more rails nodes (12 instead of 5).{{< /alert >}}

### System landscape

RA50K is deployed on 38 nodes (+ load balancers + object storage + monitoring):

{{< diagram name="gitlab-core/architectures/reference_architectures/ra50k/omnibus.png" >}}

Object storage is not represented in this diagram for simplicity. The complete diagram, with object
storage is:

{{< diagram name="gitlab-core/architectures/reference_architectures/ra50k/omnibus_with_os.png" >}}

The various clusters are isolated individually below:

#### Services Cluster

{{< diagram name="gitlab-core/architectures/reference_architectures/ra50k/omnibus_services.png" >}}

#### PostgreSQL Cluster

{{< diagram name="gitlab-core/architectures/reference_architectures/ra50k/omnibus_postgresql.png" >}}

#### Gitaly Cluster

{{< diagram name="gitlab-core/architectures/reference_architectures/ra50k/omnibus_gitaly.png" >}}

### Ingress

{{< diagram name="gitlab-core/architectures/reference_architectures/ra50k/ingress_containers.png" >}}

### Services

{{< diagram name="gitlab-core/architectures/reference_architectures/ra50k/services_containers.png" >}}

### Git Repos

{{< diagram name="gitlab-core/architectures/reference_architectures/ra50k/git_containers.png" >}}

### State

{{< diagram name="gitlab-core/architectures/reference_architectures/ra50k/state_containers.png" >}}

### Metadata DB

{{< diagram name="gitlab-core/architectures/reference_architectures/ra50k/metadata_containers.png" >}}

### Storage

{{< diagram name="gitlab-core/architectures/reference_architectures/ra50k/storage_containers.png" >}}
