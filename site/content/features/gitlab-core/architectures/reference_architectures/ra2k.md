---
title: "RA2K"
weight: 2
refs: "https://docs.gitlab.com/ee/administration/reference_architectures/2k_users.html"
layout: "architecture"
---


### System landscape

RA2K is deployed on 6 nodes (+ load balancer & object storage):

{{< diagram name="gitlab-core/architectures/reference_architectures/ra2k/omnibus.png" >}}

### Ingress

{{< diagram name="gitlab-core/architectures/reference_architectures/ra2k/ingress_containers.png" >}}

### Services

{{< diagram name="gitlab-core/architectures/reference_architectures/ra2k/services_containers.png" >}}

### Git Repos

{{< diagram name="gitlab-core/architectures/reference_architectures/ra2k/git_containers.png" >}}

### State

{{< diagram name="gitlab-core/architectures/reference_architectures/ra2k/state_containers.png" >}}

### Metadata DB

{{< diagram name="gitlab-core/architectures/reference_architectures/ra2k/metadata_containers.png" >}}

### Storage

{{< diagram name="gitlab-core/architectures/reference_architectures/ra2k/storage_containers.png" >}}
