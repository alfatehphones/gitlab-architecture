---
title: "RA3K"
weight: 3
refs: "https://docs.gitlab.com/ee/administration/reference_architectures/3k_users.html"
layout: "architecture"
---

### System landscape

ra3k is deployed on 29 nodes (+ load balancers + object storage + monitoring):

{{< diagram name="gitlab-core/architectures/reference_architectures/ra3k/omnibus.png" >}}

Object storage is not represented in this diagram for simplicity. The complete diagram, with object
storage is:

{{< diagram name="gitlab-core/architectures/reference_architectures/ra3k/omnibus_with_os.png" >}}

The various clusters are isolated individually below:

#### Services Cluster

{{< diagram name="gitlab-core/architectures/reference_architectures/ra3k/omnibus_services.png" >}}

#### PostgreSQL Cluster

{{< diagram name="gitlab-core/architectures/reference_architectures/ra3k/omnibus_postgresql.png" >}}

#### Gitaly Cluster

{{< diagram name="gitlab-core/architectures/reference_architectures/ra3k/omnibus_gitaly.png" >}}

### Ingress

{{< diagram name="gitlab-core/architectures/reference_architectures/ra3k/ingress_containers.png" >}}

### Services

{{< diagram name="gitlab-core/architectures/reference_architectures/ra3k/services_containers.png" >}}

### Git Repos

{{< diagram name="gitlab-core/architectures/reference_architectures/ra3k/git_containers.png" >}}

### State

{{< diagram name="gitlab-core/architectures/reference_architectures/ra3k/state_containers.png" >}}

### Metadata DB

{{< diagram name="gitlab-core/architectures/reference_architectures/ra3k/metadata_containers.png" >}}

### Storage

{{< diagram name="gitlab-core/architectures/reference_architectures/ra3k/storage_containers.png" >}}
