---
title: "RA10K"
weight: 10
refs: "https://docs.gitlab.com/ee/administration/reference_architectures/10k_users.html"
layout: "architecture"
---


{{< alert style="info" >}}The RA10K is based on the [RA5K](../ra5k) but redis and sentinel are
deployed on the same VMs here.{{< /alert >}}

### System landscape

ra10k is deployed on 29 nodes (+ load balancers + object storage + monitoring):

{{< diagram name="gitlab-core/architectures/reference_architectures/ra10k/omnibus.png" >}}

Object storage is not represented in this diagram for simplicity. The complete diagram, with object
storage is:

{{< diagram name="gitlab-core/architectures/reference_architectures/ra10k/omnibus_with_os.png" >}}

The various clusters are isolated individually below:

#### Services Cluster

{{< diagram name="gitlab-core/architectures/reference_architectures/ra10k/omnibus_services.png" >}}

#### PostgreSQL Cluster

{{< diagram name="gitlab-core/architectures/reference_architectures/ra10k/omnibus_postgresql.png" >}}

#### Gitaly Cluster

{{< diagram name="gitlab-core/architectures/reference_architectures/ra10k/omnibus_gitaly.png" >}}

### Ingress

{{< diagram name="gitlab-core/architectures/reference_architectures/ra10k/ingress_containers.png" >}}

### Services

{{< diagram name="gitlab-core/architectures/reference_architectures/ra10k/services_containers.png" >}}

### Git Repos

{{< diagram name="gitlab-core/architectures/reference_architectures/ra10k/git_containers.png" >}}

### State

{{< diagram name="gitlab-core/architectures/reference_architectures/ra10k/state_containers.png" >}}

### Metadata DB

{{< diagram name="gitlab-core/architectures/reference_architectures/ra10k/metadata_containers.png" >}}

### Storage

{{< diagram name="gitlab-core/architectures/reference_architectures/ra10k/storage_containers.png" >}}
