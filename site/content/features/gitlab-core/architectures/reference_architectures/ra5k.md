---
title: "RA5K"
weight: 5
refs: "https://docs.gitlab.com/ee/administration/reference_architectures/5k_users.html"
layout: "architecture"
---


{{< alert style="info" >}}The RA5K is the same as the [RA3K](../ra3k) just with larger VMs{{< /alert >}}

### System landscape

ra5k is deployed on 27 nodes (+ load balancers + object storage + monitoring):

{{< diagram name="gitlab-core/architectures/reference_architectures/ra5k/omnibus.png" >}}

Object storage is not represented in this diagram for simplicity. The complete diagram, with object
storage is:

{{< diagram name="gitlab-core/architectures/reference_architectures/ra5k/omnibus_with_os.png" >}}

The various clusters are isolated individually below:

#### Services Cluster

{{< diagram name="gitlab-core/architectures/reference_architectures/ra5k/omnibus_services.png" >}}

#### PostgreSQL Cluster

{{< diagram name="gitlab-core/architectures/reference_architectures/ra5k/omnibus_postgresql.png" >}}

#### Gitaly Cluster

{{< diagram name="gitlab-core/architectures/reference_architectures/ra5k/omnibus_gitaly.png" >}}

### Ingress

{{< diagram name="gitlab-core/architectures/reference_architectures/ra5k/ingress_containers.png" >}}

### Services

{{< diagram name="gitlab-core/architectures/reference_architectures/ra5k/services_containers.png" >}}

### Git Repos

{{< diagram name="gitlab-core/architectures/reference_architectures/ra5k/git_containers.png" >}}

### State

{{< diagram name="gitlab-core/architectures/reference_architectures/ra5k/state_containers.png" >}}

### Metadata DB

{{< diagram name="gitlab-core/architectures/reference_architectures/ra5k/metadata_containers.png" >}}

### Storage

{{< diagram name="gitlab-core/architectures/reference_architectures/ra5k/storage_containers.png" >}}
