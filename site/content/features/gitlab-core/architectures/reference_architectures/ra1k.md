---
title: "RA1K"
weight: 1
refs: https://docs.gitlab.com/ee/administration/reference_architectures/1k_users.html
layout: "architecture"
---

### System landscape

RA1K is deployed on 1 node only (+ object storage):

{{< diagram name="gitlab-core/architectures/reference_architectures/ra1k/omnibus.png" >}}

### Ingress

{{< diagram name="gitlab-core/architectures/reference_architectures/ra1k/ingress_containers.png" >}}

### Services

{{< diagram name="gitlab-core/architectures/reference_architectures/ra1k/services_containers.png" >}}

### Git Repos

{{< diagram name="gitlab-core/architectures/reference_architectures/ra1k/git_containers.png" >}}

### State

{{< diagram name="gitlab-core/architectures/reference_architectures/ra1k/state_containers.png" >}}

### Metadata DB

{{< diagram name="gitlab-core/architectures/reference_architectures/ra1k/metadata_containers.png" >}}

### Storage

{{< diagram name="gitlab-core/architectures/reference_architectures/ra1k/storage_containers.png" >}}
