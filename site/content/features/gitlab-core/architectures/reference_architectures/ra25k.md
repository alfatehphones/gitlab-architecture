---
title: "RA25K"
weight: 25
refs: "https://docs.gitlab.com/ee/administration/reference_architectures/25k_users.html"
layout: "architecture"
---

{{< alert style="info" >}}The RA25K is the same as the [RA10K](../ra10k) just with larger VMs and more rails nodes (5 instead of 3).{{< /alert >}}

### System landscape

RA25K is deployed on 31 nodes (+ load balancers + object storage + monitoring):

{{< diagram name="gitlab-core/architectures/reference_architectures/ra25k/omnibus.png" >}}

Object storage is not represented in this diagram for simplicity. The complete diagram, with object
storage is:

{{< diagram name="gitlab-core/architectures/reference_architectures/ra25k/omnibus_with_os.png" >}}

The various clusters are isolated individually below:

#### Services Cluster

{{< diagram name="gitlab-core/architectures/reference_architectures/ra25k/omnibus_services.png" >}}

#### PostgreSQL Cluster

{{< diagram name="gitlab-core/architectures/reference_architectures/ra25k/omnibus_postgresql.png" >}}

#### Gitaly Cluster

{{< diagram name="gitlab-core/architectures/reference_architectures/ra25k/omnibus_gitaly.png" >}}

### Ingress

{{< diagram name="gitlab-core/architectures/reference_architectures/ra25k/ingress_containers.png" >}}

### Services

{{< diagram name="gitlab-core/architectures/reference_architectures/ra25k/services_containers.png" >}}

### Git Repos

{{< diagram name="gitlab-core/architectures/reference_architectures/ra25k/git_containers.png" >}}

### State

{{< diagram name="gitlab-core/architectures/reference_architectures/ra25k/state_containers.png" >}}

### Metadata DB

{{< diagram name="gitlab-core/architectures/reference_architectures/ra25k/metadata_containers.png" >}}

### Storage

{{< diagram name="gitlab-core/architectures/reference_architectures/ra25k/storage_containers.png" >}}
