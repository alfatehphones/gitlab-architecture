---
title: "Development"
refs: "https://docs.gitlab.com/ee/development/architecture"
layout: "architecture"
---

### System landscape

GitLab Development architecture is the architecture used in Development, usually with the [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit).

{{< diagram name="gitlab-core/architectures/development/dev.png" >}}

### Ingress

{{< diagram name="gitlab-core/architectures/development/Ingress_Containers.png" >}}

### Services

{{< diagram name="gitlab-core/architectures/development/Services_Containers.png" >}}

### Git Repos

{{< diagram name="gitlab-core/architectures/development/Git_Containers.png" >}}

### State

TODO

### Metadata DB

{{< diagram name="gitlab-core/architectures/development/Metadata_Containers.png" >}}

### Storage

TODO
